// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBysJvoaQn6oaDFvsHbKdFdl7WFfg11APg",
    authDomain: "todolistappp.firebaseapp.com",
    databaseURL: "https://todolistappp.firebaseio.com",
    projectId: "todolistappp",
    storageBucket: "todolistappp.appspot.com",
    messagingSenderId: "571029833955"
  }
};
